import org.junit.Test;
import static org.junit.Assert.*;

public class Main {

    /*������� ����������� �� ���� ����� Минимальное из трех чмсел */
    public static int minOfThree(int a, int b, int c) {
        if (a > b)
     /*Неверная проверка - заменить на (a < b)*/
            if (a < c) return a;
            else return c;
        else if (b < c) return b;
        else return c;
    }
    @Test
    public void testMinOfThree() {
        assertEquals(0, Main.minOfThree(2, 0, 1));
    }

    /*��������� ��������� ��������� ����� Факториал числа*/
    public static int factorial(int n) {
        /*Отсутствует проверка на возможность отрицательного значения аргумента */
        int fact = 1;
        while (n > 0) {
            fact = fact * n;
            n++;
        }
        return fact;
    }
    @Test
    public void testFactorialThrowsException() throws IllegalArgumentException {
        Main.factorial(-1);
    }


    /*���������� ��� ������ Сравнение строк*/
    public static boolean compareString(String firstString, String secondString) {
        return firstString == secondString;
        /*Сравнение ссылок,а не их содержимого */
    }

    @Test
    public void testCompareString1() {
        assertTrue(Main.compareString(new String("abc"), new String("abc")));
            }


    /*��������� ���������� ������ � ���� Склеивание строк*/
    public static String concatenateString(String... strings) {
        /* Метод не соединяет строки
        * Нет проверки на null*/
        String result = "";
        for (String s : strings)
            result = s;
        return result;
    }

    @Test
    public void testConcatenateString1() {
        assertEquals("abc", Main.concatenateString("a", "b", "c"));
    }

    @Test
    public void testConcatenateStringThrowsException() throws IllegalArgumentException {
        System.out.println(Main.concatenateString(null));
    }


    /*������� ������� � ���������� ������� Минимальное число в одномерном массиве*/
        public static int findMinInArray(int[] array) {
/*i=array.length; не попадает в границы массива
* отсутствует проверка на null*/
        int min = array[0];
        for (int i = 1; i <= array.length; i++) if (array[i] < min) min = array[i];
        return min;
    }
    @Test
    public void testFindMinInArray1() {
        int[] array = {1, 2, 3, 4, 5};
        assertEquals(1, Main.findMinInArray(array));
    }

    @Test
    public void testFindMinInArrayThrowsException() throws IllegalArgumentException {
        Main.findMinInArray(null);
    }

    /*��������� ������ �� �����������Сортировка массива по возростанию*/
    public static int[] sortArray(int[] array) {
        /*Вместо возрастания сортировкапо убыванию
        * нет проверки на null
        * */
        for (int i = array.length; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                if (array[j] < array[j + 1]) {
                    int buffer = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = buffer;
                }
            }
        }
        return array;
    }
    @Test
    public void testSortArray1() {
        int[] unsortedArray = {5, 2, 1, 3, 4};
        int[] sortedArray = {1, 2, 3, 4, 5};
        assertArrayEquals(sortedArray, Main.sortArray(unsortedArray));
    }

    @Test
    public void testSortArrayThrowsException() throws IllegalArgumentException {
        Main.sortArray(null);
    }

    /*������� ����� ���������, ����������� ���� ������� ��������� Сумма элементов ниже диагонали*/
    public static int sumOfElementsUnderMainDiagonal(int[][] matrix) {
        /*Нет проверки аргумента на null
        * Матрица не проверяется накорректность
        * В услови выхода из цикла - заменить (j > i) на (j >= i)*/
        int sum = 0;
        for (int i = 0; i < matrix.length; i++)
            for (int j = 0; j < matrix.length; j++) {
                if (j > i) break;
                sum += matrix[i][j];
            }
        return sum;
    }
    @Test
    public void testSumOfElementsUnderMainDiagonalNullArgumentThrowsException() throws IllegalArgumentException {
        Main.sumOfElementsUnderMainDiagonal(null);
    }

    @Test
    public void testSumOfElementsUnderMainDiagonalIncorrectThrowsException() throws IllegalArgumentException {
        int[][] array = {   {},
                null,
                {1, 2}   };
        Main.sumOfElementsUnderMainDiagonal(array);
    }

    @Test
    public void testSumOfElementsUnderMainDiagonal1() {
        int[][] array = {   {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}  };
        assertEquals(19, Main.sumOfElementsUnderMainDiagonal(array));
    }


    /*������� ���������� �����, ���������� ��������� ������ � ������� Числа степени двойки в матрице*/
    public static int countPowerOfTwo(int[][] matrix) {
        /*Нет проверки аргумента на null
        * Матрица не проверяется накорректность
        * неправильое условиебинарной проверки - заменить "|" на "&"
        * Нет роверки элементов на нулевое значение*/
        int count = 0;
        for (int i = 0; i < matrix.length; i++)
            for (int j = 0; j < matrix[0].length; j++)
                if ((matrix[i][j] | (matrix[i][j] - 1)) == 0) count++;

        return count;
    }
    @Test
    public void testCountPowerOfTwoNullArgumentThrowsException() throws IllegalArgumentException {
        Main.countPowerOfTwo(null);
    }

    @Test
    public void testCountPowerOfTwoIncorrectMatrixThrowsException() throws IllegalArgumentException {
        int[][] array = {   {},
                null,
                {1, 2}   };
        Main.countPowerOfTwo(array);
    }

    @Test
    public void testCountPowerOfTwo1() {
        int[][] array = {   {0, 2, 4},
                {8, 15, 16},
                {32, 64, 127}   };
        assertEquals(6, Main.countPowerOfTwo(array));
    }

    /*����� ������ 1000 ����������� Сон на 1000милисекунд*/
    public static void sleepFor1000() throws InterruptedException {
        Thread.sleep(1000);
        /*В єтом методе Поток заснет на 1 секунду */
    }

    /*����� ������� ���������� Выброс исключения*/
    public static void throwException() throws Exception {
        throw new Exception();
        /*Метод бросает исключение*/
    }
}